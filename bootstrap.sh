cd ~/.config/
echo 'creating config'
mkdir arm64build; cd arm64build
echo 'cloning repo'
git clone https://gitlab.com/thebluedev/arm64build-win.git
cd arm64build-win
echo 'running bootstrapper...'
chmod +x ./index
chmod +x ./exec
echo 'prepare to exit after completion message'
./exec & ./index
